function post(url, data = {}) {
  return new Promise(function (resolve, reject) {
    wx.request({
      header: {
        // 'content-type': 'application/json' 默认值get方式
        'content-type': 'application/x-www-form-urlencoded' // post方式
      },
      method: "POST",
      url: url,
      data: data,
      success(res) {
        resolve(res);
      },
      fail(err) {
        reject(err)
      }
    })
  })
}

function get(url, data = {}) {
  return new Promise(function (resolve, reject) {
    wx.request({
      header: {
        'content-type': 'application/json' // 默认值get方式
        // 'content-type': 'application/x-www-form-urlencoded' // post方式
      },
      method: "GET",
      url: url,
      data: data,
      success(res) {
        resolve(res);
      },
      fail(err) {
        reject(err)
      }
    })
  })
}
module.exports = {
  post:post,
  get:get
}