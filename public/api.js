const API = {
  getStudentInfo: "http://xqzyyds.top:8083/student/detail",
  login: "http://xqzyyds.top:8083/student/login",
  loginByWX: "http://xqzyyds.top:8083/student/login/wx",
  bindWx: "http://xqzyyds.top:8083/student/bind",
  updatePassword: "http://xqzyyds.top:8083/student",
  cardPay: "http://xqzyyds.top:8083/student/card",
  getElectricityPurchaseRecords: "http://xqzyyds.top:8083/room/electricity/purchase/records",
  bindRoom: "http://xqzyyds.top:8083/room/bind",
  getRoom: "http://xqzyyds.top:8083/room",  
  purchaseElectricity: "http://xqzyyds.top:8083/room/electricity/money",
  getElectricityUsedRecords: "http://xqzyyds.top:8083/room/electricity/used/records",
  listPayMessages: "http://xqzyyds.top:8083/message/pay/list",
  listSystemMessages: "http://xqzyyds.top:8083/message/system/list",
  listGuetNews: "http://xqzyyds.top:8083/news/guetnews/list",
  listNotice: "http://xqzyyds.top:8083/notice/list",
  addCourse: "http://xqzyyds.top:8083/course",
  getCurriculum: "http://xqzyyds.top:8083/curriculum",
  getStartSchoolTime: "http://xqzyyds.top:8083/curriculum/start",
  listTerm: "http://xqzyyds.top:8083/term",
  getPhoneNumber: "http://xqzyyds.top:8083/wx/getPhoneNumber"
};
module.exports = API;
