Component({
  /**
   * 存放要从父组件中接收的数据
   */
  properties: {
    //要接收的数据的名称
    // aaa:{
    //   //type  标书要接收的数据类型       
    //   type:String,
    //   //value  默认值
    //   value:""
    // }
    tabs: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 页面js存放回调函数存放在data同层级下
   组件的js文件中存放时间的回调函数必须存放在method中
   */
  methods: {
    handleItemTap(e) {
      // 绑定点击事件。  获取被点击的索引   获取原数组  
      // 对数组循环（给每一个循环项 选中属性改为false  给当前索引的项添加激活选中效果）

      //获取索引
      const {
        index
      } = e.currentTarget.dataset;
      // ("父组件自定义事件名称"要传递的参数)
      this.triggerEvent("itemChange", {
        index
      });
      //获取data中的数组
      //  let {tabs}=this.data;  //let tabs=this.data.tabs;  一样
      //循环数组
      //[].forEach   遍历数组  遍历数组的时候修改了v 也会导致原数组被修改
      //  tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);
      //  this.setData({
      //  tabs
      //  })
    }
  }
})