// app.js
App({
  onLaunch() {
    // 跳转到登录页面
    var studentInfo = wx.getStorageSync('studentInfo');
    console.log(studentInfo);
    if (studentInfo == null || studentInfo == "") {
      wx.redirectTo({
        url: '/pages/login/login',
      })
    } else {
      wx. switchTab({      
        url: '/pages/index/index'
      });  
    }


    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null
  },
  towxml: require('/towxml/index')
})
