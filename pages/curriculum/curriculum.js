const request = require('../../public/request');
const API = require('../../public/api')
import {
  updateCurriculum
} from '../../utils/initCurriculum';
import {
  getCurricumOfWeek
} from '../../utils/initCurriculum';
import {
  getCurrentTerm
} from '../../utils/initCurriculum';
import {
  getCurrWeekList
} from '../../utils/util';
import {
  formateDate
} from '../../utils/util';
import {
  getCurrentWeekNumber
} from '../../utils/util';
import {
  getFormattedWeekByNumber
} from '../../utils/util';
import {
  convertTimestampToFormattedDate,
  getMonthByNumber
} from '../../utils/util';


const weeks = []
for (let i = 1; i <= 31; i++) {
  weeks.push(i)
}

Page({
  data: {
    time: {
      one: [{
          index: 1,
          timeStart: '08:00',
          timeEnd: '10:00'
        },
        {
          index: 2,
          timeStart: '10:25',
          timeEnd: '12:00'
        }
      ],
      two: [{
          index: 3,
          timeStart: '14:30',
          timeEnd: '16:05'
        },
        {
          index: 4,
          timeStart: '16:30',
          timeEnd: '18:05'
        },
      ],
      three: [{
        index: '晚',
        timeStart: '19:30',
        timeEnd: '20:15'
      }]
    },
    schedule: [],
    weekList: [],
    isShow: false,
    isShowSelectedWeek: false,
    current: {},
    terms: [],
    term: {},
    weeks: weeks,
    month: 11,
    week: 2,
    value: [3, 12],
  },

  /**
   * 监听“选择第几周的弹窗”，然后构建那周的课程表信息
   * @param {*} e 
   */
  bindChange: async function (e) {
    const val = e.detail.value
    const term = this.data.terms[val[0]];
    const currentWeek = this.data.weeks[val[1]];
    console.log(this.data.term);
    console.log(term);
    if (this.data.term != term) {
      let sno = wx.getStorageSync('studentInfo').sno;
      await updateCurriculum(sno, term.id);
    }
    console.log(currentWeek);
    this.setData({
      term: term,
      week: currentWeek
    });
    // 构建该星期的课程
    let schedule = await getCurricumOfWeek(currentWeek, term);
    this.setData({
      schedule: schedule
    });
    // 获取该学期的开学时间
    let startSchoolDate = convertTimestampToFormattedDate(term.startTime);
    const weekList = getFormattedWeekByNumber(startSchoolDate, currentWeek);
    console.log(weekList); // 包含日期、星期和是否为当前日期的周列表
    // 得到结果后保存起来，然后在页面实现
    this.setData({
      weekList: weekList
    })

    const targetMonth = getMonthByNumber(startSchoolDate, currentWeek);
    console.log(targetMonth); // 目标周所在的月份，格式为 'MM'
    this.setData({
      month: targetMonth
    })

  },

  /**
   * 保存当前点击的课程信息
   * @param {*} e 
   */
  getDetail(e) {
    let {
      item
    } = e.currentTarget.dataset;
    this.setData({
      current: item,
      isShow: true
    })
  },

  /**
   * 课程详情的弹窗
   */
  close() {
    this.setData({
      isShow: false
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let time = new Date(), // 获取当前时间
      list = getCurrWeekList(time), // 根据当前时间获取这周
      weekList = []
    list.forEach(item => { // 对这周进行初始化
      weekList.push({
        day: [item.split('-')[1], item.split('-')[2]].join('-'),
        week: "星期" + "日一二三四五六".charAt((new Date(item)).getDay()),
        isCurr: formateDate(time) == item
      })
    });
    // 得到结果后保存起来，然后在页面实现
    this.setData({
      weekList,
    })
  },

  /**
   * 选择第几周的弹窗开关
   */
  selectedWeek() {
    this.setData({
      isShowSelectedWeek: !this.data.isShowSelectedWeek
    })
  },

  /**
   * 恢复到当前学期当前周
   */
  async recoverCurrentWeek() {
    //显示加载界面
    wx.showLoading({ // 显示加载中loading效果 
      title: "加载中",
      mask: true //开启蒙版遮罩
    });
    // ①获取当前学期
    var currentTerm = {};
    await getCurrentTerm(this.data.terms).then(item => {
      currentTerm = item;
    })
    console.log(currentTerm);
    // 获取该学期的开学时间
    let startSchoolDate = convertTimestampToFormattedDate(currentTerm.startTime);
    const sno = wx.getStorageSync('studentInfo').sno;
    // ②构建当前学期课表
    await updateCurriculum(sno, currentTerm.id)
    // ③构建这周课表
    let time = new Date(), // 获取当前时间
      list = getCurrWeekList(time), // 根据当前时间获取这周
      weekList = []
    // 对这周进行初始化
    list.forEach(item => {
      weekList.push({
        day: [item.split('-')[1], item.split('-')[2]].join('-'),
        week: "星期" + "日一二三四五六".charAt((new Date(item)).getDay()),
        isCurr: formateDate(time) == item
      })
    });

    // 格式化今天的日期 2023-10-28
    const today = formateDate(new Date());
    // 开学那天是第一周，根据今天日期和开学日期计算今天是第几周
    const currentWeek = getCurrentWeekNumber(startSchoolDate, today);
    // 开学那天是第一周，根据今天日期和开学日期计算今天是哪一月
    const currentMonth = getMonthByNumber(startSchoolDate, currentWeek);
    // 获取这周的课表
    const schedule = await getCurricumOfWeek(currentWeek, currentTerm);
    // 设置选择框的value值
    const termIndex = this.data.terms.findIndex(item => item.id === currentTerm.id);
    setTimeout(() => {
      this.setData({
        value: [termIndex, currentWeek - 1],
      })
    }, 0);
    // 得到结果后保存起来，然后在页面实现
    this.setData({
      isShowSelectedWeek: false,
      weekList: weekList,
      week: currentWeek,
      month: currentMonth,
      term: currentTerm,
      schedule: schedule
    });
    //隐藏加载界面
    wx.hideLoading();
  },

  /**
   * 
   */
  async lastOrNextWeek(e) {
    let {
      num
    } = e.currentTarget.dataset;
    num = Number(num);
    const term = this.data.term;
    const currentWeek = this.data.week + num;
    console.log(currentWeek);
    if (currentWeek <= 0 || currentWeek >= 30) {
      return;
    }
    // 构建该星期的课程
    let schedule = await getCurricumOfWeek(currentWeek, term);
    this.setData({
      schedule: schedule
    });
    // 获取该学期的开学时间
    let startSchoolDate = convertTimestampToFormattedDate(term.startTime);
    const weekList = getFormattedWeekByNumber(startSchoolDate, currentWeek);
    console.log(weekList); // 包含日期、星期和是否为当前日期的周列表
    const targetMonth = getMonthByNumber(startSchoolDate, currentWeek);
    console.log(targetMonth); // 目标周所在的月份，格式为 'MM'
    // 得到结果后保存起来，然后在页面实现
    this.setData({
      month: targetMonth,
      week: currentWeek,
      weekList: weekList
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    //显示加载界面
    wx.showLoading({ // 显示加载中loading效果 
      title: "加载中",
      mask: true //开启蒙版遮罩
    });
    // 获取学期列表
    let {
      data: terms
    } = await request.get(API.listTerm);
    console.log("Ready----学期列表");
    console.log(terms.data);
    // 获取当前学期
    var currentTerm = {};
    await getCurrentTerm(terms.data).then(item => {
      currentTerm = item;
    })
    // 获取该学期的开学时间
    let startSchoolDate = convertTimestampToFormattedDate(currentTerm.startTime);
    console.log(startSchoolDate);
    // 获取今天的日期 2023-10-28
    let today = formateDate(new Date());
    // 开学那天是第一周，根据今天日期和开学日期计算今天是第几周
    let currentWeek = getCurrentWeekNumber(startSchoolDate, today);
    // 开学那天是第一周，根据今天日期和开学日期计算今天是哪一月
    console.log(currentTerm);
    const currentMonth = getMonthByNumber(startSchoolDate, currentWeek);

    // 获取当前这周的课表信息
    let schedule = await getCurricumOfWeek(currentWeek, currentTerm);
    // 设置选择框的value值
    const termIndex = terms.data.findIndex(item => item.id === currentTerm.id);
    setTimeout(() => {
      this.setData({
        value: [termIndex, currentWeek - 1],
      })
    }, 0);
    // 保存相应的数据
    this.setData({
      term: currentTerm,
      week: currentWeek,
      month: currentMonth,
      schedule: schedule,
      terms: terms.data
    });
    //隐藏加载界面
    wx.hideLoading();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  async onReady() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})