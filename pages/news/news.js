const request = require('../../public/request')
const API = require('../../public/api');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
        id: 0,
        name: "桂电新闻",
        isActive: true,
        dateList: []
      },
      {
        id: 1,
        name: "通知公告",
        isActive: false,
        dateList: []
      },
    ],
  },

  async toGuetNews(e) {
    let tabsindex = this.data.tabs[0].isActive ? 0 : 1;
    let index = e.currentTarget.dataset.index;
    let params = this.data.tabs[tabsindex].dataList[index];
    let param = '?' + 'title=' + params.title + '&content=' + params.content + '&createTime=' + params.createTime + '&updateTime=' + params.updateTime;
    let url = '../newsGuet/newsGuet' + param;
    wx.navigateTo({
      url: url
    })
  },

  handleItemChange(e) {
    const {
      index
    } = e.detail;
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    this.setData({
      tabs
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    // 获取桂电新闻
    const {
      data: news1
    } = await request.get(API.listGuetNews);
    console.log(news1);
    this.setData({
      'tabs[0].dataList': news1.data
    });
    // 获取周边新闻
    const {
      data: news2
    } = await request.get(API.listNotice);
    console.log(news2);
    this.setData({
      'tabs[1].dataList': news2.data
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})