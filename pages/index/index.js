const request = require('../../public/request')
const API = require('../../public/api');
// 获取应用实例
const app = getApp()

Page({
  data: {
    banner: ['http://images.xqzyyds.top/miniprogram/images/banner1.png', 'http://images.xqzyyds.top/miniprogram/images/banner2.png', 'http://images.xqzyyds.top/miniprogram/images/banner3.png'],
    indexConfig: [{
        icon: 'http://images.xqzyyds.top/miniprogram/images/card_home.png',
        text: '一卡通',
        url: '../card/card'
      },
      {
        icon: 'http://images.xqzyyds.top/miniprogram/images/electricity_home.png',
        text: '宿舍用电',
        url: '../electricity/electricity',
      },
      {
        icon: 'http://images.xqzyyds.top/miniprogram/images/message_home.png',
        text: '消息中心',
        url: '../message/message',
      },
      {
        icon: 'http://images.xqzyyds.top/miniprogram/images/news_home.png',
        text: '新闻公告',
        url: '../news/news',
      }
    ],
    tabs: [{
        id: 0,
        name: "桂电新闻",
        isActive: true,
        dateList: []
      },
      {
        id: 1,
        name: "通知公告",
        isActive: false,
        dateList: []
      },
    ],
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },

  toDetail(e) {
    const studentInfo = wx.getStorageSync('studentInfo');
    const url = e.currentTarget.dataset.url;
    if (studentInfo) {
      wx.navigateTo({
        url,
      })
    } else {
      wx.showToast({
        icon: 'none',
        title: '请前往个人中心登录',
      })
    }
  },

  handleItemChange(e) {
    const {
      index
    } = e.detail;
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    this.setData({
      tabs
    })
  },

  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },

  /**
   * 进入新闻详情页
   * @param {*} e 
   */
  async toGuetNews(e) {
    let tabsindex = this.data.tabs[0].isActive ? 0 : 1;
    let index = e.currentTarget.dataset.index;
    let params = this.data.tabs[tabsindex].dataList[index];
    let param = '?' + 'title=' + params.title + '&content=' + params.content + '&createTime=' + params.createTime + '&updateTime=' + params.updateTime;
    let url = '../newsGuet/newsGuet' + param;
    wx.navigateTo({
      url: url
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    // 获取桂电新闻
    const {
      data: news1
    } = await request.get(API.listGuetNews);
    console.log(news1);
    this.setData({
      'tabs[0].dataList': news1.data
    });
    // 获取周边新闻
    const {
      data: news2
    } = await request.get(API.listNotice);
    console.log(news2);
    this.setData({
      'tabs[1].dataList': news2.data
    });
  }
})