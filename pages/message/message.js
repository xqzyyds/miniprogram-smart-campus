const request = require('../../public/request')
const API = require('../../public/api');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs:[
      {
        id:0,
        name:"支付消息",
        isActive:true,
        dataList: []
      },
      {
        id:1,
        name:"系统通知",
        isActive:false,
        dataList: []
      },
    ],
  },
  
  handleItemChange(e){
    const {index}=e.detail;
    let {tabs}=this.data;  
    tabs.forEach((v,i)=>i===index?v.isActive=true:v.isActive=false);
    this.setData({
      tabs
   })
  },

  toMessagePay(e) {
    let index = e.currentTarget.dataset.index;
    console.log(e.currentTarget.dataset.index);
    let params = this.data.tabs[0].dataList[index];
    let param = '?' + 'money=' + params.money + '&orderId=' + params.orderId + '&payMethod=' + params.payMethod + '&payStatus=' + params.payStatus + '&payTime=' + params.payTime + '&payType=' + params.payType + '&targetAccount=' + params.targetAccount;
    let url = '../messagePay/messagePay' + param;
    wx.navigateTo({
      url: url
    })
  },

  toMessageSystem(e) {
    let index = e.currentTarget.dataset.index;
    console.log(e.currentTarget.dataset.index);
    let params = this.data.tabs[1].dataList[index];
    let param = '?' + 'version=' + params.version + '&content=' + params.content + '&time=' + params.updateTime;
    let url = '../messageSystem/messageSytem' + param;
    wx.navigateTo({
      url: url
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    var data = {
      sno: wx.getStorageSync('studentInfo').sno
    }
    const { data: res}  = await request.get(API.listPayMessages, data);
    console.log(res);
    this.setData({
      'tabs[0].dataList': res.data
    });
    const { data: systemMessages}  = await request.get(API.listSystemMessages);
    console.log(systemMessages);
    this.setData({
      'tabs[1].dataList': systemMessages.data
    });
    console.log(this.data.tabs[1].dataList);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})