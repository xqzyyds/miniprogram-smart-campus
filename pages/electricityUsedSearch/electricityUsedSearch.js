const request = require('../../public/request')
const API = require('../../public/api');
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
  return new(P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : new P(function (resolve) {
        resolve(result.value);
      }).then(fulfilled, rejected);
    }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tableColumns: [{
      title: "日期",
      key: "currentTime",
    }, {
      title: "剩余电量",
      key: "battery",
    }],
    dataList: [],
    pageNum: 1,
    pageSize: 15,
    pageCount: 1,
    getListLoading: false,
    tableHeight: "100%",
    roomName: ''
  },

  async getList() {
    await this.getRoomName();
    var roomName = this.data.roomName;
    return __awaiter(this, void 0, void 0, function* () {
      try {
        const {
          pageNum,
          pageSize,
          pageCount,
          dataList,
          getListLoading
        } = this.data;
        if (pageNum > pageCount)
          return;
        if (getListLoading)
          return;
        this.setData({
          getListLoading: true,
        });

        var that = this;
        wx.request({
          url: API.getElectricityUsedRecords,
          header: {
            'content-type': 'application/json' // 默认值get方式
            // 'content-type': 'application/x-www-form-urlencoded' // post方式
          },
          timeout: 3000,
          method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
          data: {
            roomName: roomName,
          },
          success: function (res) {
            console.log(res.data);
            if (res.data.code == 200) {
              that.data.dataList = res.data.data
              that.setData({
                dataList: dataList.concat(res.data.data),
                pageCount: res.data.data.length,
                getListLoading: false,
                pageNum: res.data.data.length > 0 ? pageNum + 1 : pageNum,
              });
            } else {
              
            }
          },
          fail: function () {
            wx.showToast({
              title: "网络连接失败",
              duration: 3000
            })
          },
        });
      } catch (e) {
        this.setData({
          getListLoading: false,
        });
        console.log(e);
      }
    });
  },

  async getRoomName() {
    var sno = wx.getStorageSync('studentInfo').sno;
    var data = {
      sno: sno
    }
    const { data: res} = await request.get(API.getRoom, data)
    console.log(res);
    this.setData({
      roomName: res.data
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})