// pages/newsGuet/newsGuet.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,					// 判断是否尚在加载中
    title: '',
    content: '',
    createTime: '',
    updateTime: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const app = getApp();
    console.log('app');
    console.log(app);
    // 回调函数中完成更新数据的逻辑
    let result = app.towxml(options.content, 'markdown',{          
      theme:'light',                    
      events:{                    
        tap:(e)=>{
          console.log('tap',e);
        }
      }
    });
    // 更新解析数据
    this.setData({
      title: options.title,
      createTime: options.createTime,
      updateTime: options.updateTime,
      content:result,
      isLoading: false
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})