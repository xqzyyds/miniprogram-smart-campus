const request = require('../../public/request')
const API = require("../../public/api");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sno: '',
    roomName: ''
  },

  /**
   * 自动根据学号获取宿舍房间号
   * @param {*} e 
   */
  watchUsername(e) {
    var that = this;
    var sno = e.detail.value;
    this.setData({
      sno: sno
    })
    wx.request({
      url: API.getRoom,
      header: {
        'content-type': 'application/json' // 默认值get方式
        // 'content-type': 'application/x-www-form-urlencoded' // post方式
      },
      timeout: 3000,
      method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      data: {
        sno: sno,
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.code == 200) {
          that.setData({
            roomName: res.data.data
          })
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络连接失败",
          duration: 3000
        })
      },
    });
  },

  /**
   * 监控宿舍房间号输入框
   * @param {*} e 
   */
  getRoom(e) {
    this.setData({
      roomName: e.detail.value
    });
  },

  /**
   * 跳转到用电查询
   */
  async toElectricityUsedSearch() {
    let sno = wx.getStorageSync('studentInfo').sno;
    let data = {
      sno: sno
    }
    const {
      data: res
    } = await request.get(API.getRoom, data);
    console.log(res);
    if ("宿舍房间号不存在" == res.message) {
      wx.showModal({
        title: '提示',
        content: res.message
      });
      return;
    }
    wx.navigateTo({
      url: '../electricityUsedSearch/electricityUsedSearch',
    })
  },

  /**
   * 跳转到购电查询
   */
  async toElectricityBuySearch() {
    let sno = wx.getStorageSync('studentInfo').sno;
    let data = {
      sno: sno
    }
    const {
      data: res
    } = await request.get(API.getRoom, data);
    console.log(res);
    if ("宿舍房间号不存在" == res.message) {
      wx.showModal({
        title: '提示',
        content: res.message
      });
      return;
    }
    wx.navigateTo({
      url: '../electricityBuySearch/electricityBuySearch',
    })
  },

  /**
   * 跳转到电费充值
   */
  toElectricityBuy() {
    wx.navigateTo({
      url: '../electricityBuy/electricityBuy',
    })
  },

  /**
   * 宿舍房间号绑定
   */
  async binding() {
    //显示加载界面
    wx.showLoading({ // 显示加载中loading效果 
      title: "加载中",
      mask: true //开启蒙版遮罩
    });
    var that = this;
    var roomName = this.data.roomName;
    var sno = this.data.sno;
    if (sno == null || sno == '') {
      wx.showModal({
        title: '提示',
        content: '学号不能为空！'
      })
      return;
    }
    if (roomName == null || roomName == '') {
      wx.showModal({
        title: '提示',
        content: '宿舍号不能为空！'
      })
      return;
    }
    let data = {
      sno: sno,
      roomName: roomName
    }
    const {
      data: res
    } = await request.post(API.bindRoom, data);
    console.log(res);
    if (res.message == "用户名不存在" || res.message == "宿舍房间号不存在" || res.message == "宿舍号绑定失败") {
      wx.showModal({
        title: '提示',
        content: res.message
      })
    } else {
      wx.showToast({
        title: res.data,
        duration: 2000,
        icon: 'success',
        mask: true
      })
    }
    //隐藏加载界面
    wx.hideLoading();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})