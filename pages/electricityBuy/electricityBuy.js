const API = require("../../public/api");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAliPay: false,
    money: 20,
    sno: "",
    roomName: "",
    showModal: false
  },

  /**
   * 用户输入学号后，如果已经绑定了宿舍号，会自动根据学号获取房间号并自动填充在宿舍输入框里
   * @param {*} e 
   */
  getUsername(e) {
    var that = this;
    var sno = e.detail.value;
    this.setData({
      sno: sno
    })
    wx.request({
      url: API.getRoom,
      header: {
        'content-type': 'application/json' // 默认值get方式
        // 'content-type': 'application/x-www-form-urlencoded' // post方式
      },
      timeout: 3000,
      method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      data: {
        sno: sno,
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.code == 200) {
          that.setData({
            roomName: res.data.data
          })
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络连接失败",
          duration: 3000
        })
      },
    });
  },

  /**
   * 监控用户输入的房间号
   * @param {*} e 
   */
  watchRoom(e) {
    this.setData({
      roomName: e.detail.value
    })
  },

  /**
   * 支付宝支付
   */
  isAliPay() {
    this.setData({
      isAliPay: true
    })
  },

  /**
   * 微信支付
   */
  isWxPay() {
    this.setData({
      isAliPay: false
    })
  },

  /**
   * 监控支付金额--输入框（用户自己输入）
   * @param {*} e 
   */
  watchMoney: function(e) {
    this.setData({
      money: e.detail.value
    })
  },

  /**
   * 监控支付金额--选择型20元、50元、100元
   * @param {*} e 
   */
  moneyChange(e) {
    this.setData({
      money: e.currentTarget.dataset.money
    })
  },

  /**
   * 显示弹窗
   */
  pay() {
    let money = this.data.money;
    if (money <= 0) {
      wx.showModal({
        title: '提示',
        content: '请输入正确的金额！'
      })
    } else if (money >= 10000) {
      wx.showModal({
        title: '提示',
        content: '充值的金额太大！'
      })
    } else {
      this.setData({
        showModal: true
      })
    }
  },

  /**
   * 充值，提交表单
   */
  formSubmit() {
    let sno = this.data.sno;
    let roomName = this.data.roomName;
    let money = this.data.money;
    let payMethod = this.data.isAliPay ? '支付宝' : '微信支付';

    if (sno == null || sno == '') {
      wx.showModal({
        title: '提示',
        content: '学号不能为空！'
      })
      return;
    }
    if (roomName == null || roomName == '') {
      wx.showModal({
        title: '提示',
        content: '宿舍号不能为空！'
      })
      return;
    }
    if (money == null || money <= 0) {
      wx.showModal({
        title: '提示',
        content: '缴费金额输入格式错误'
      })
      return;
    }
    
    wx.request({
      url: API.purchaseElectricity,
      header: {
        // 'content-type': 'application/json' // 默认值get方式
        'content-type': 'application/x-www-form-urlencoded' // post方式
      },
      timeout: 3000,
      method: 'put', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      data: {
        sno: sno,
        roomName: roomName,
        money: money,
        payMethod: payMethod
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.code == 200) {
          wx.showToast({
            title: '支付成功',
            image: '../../images/pay_success.png',
            duration: 3000,
            mask: true
          })
        } else {
          wx.showModal({
            title: '提示',
            content: res.data.message
          })
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络连接失败",
          duration: 3000
        })
      },
    });
    this.setData({
      showModal: false
    })
  },

  /**
   * 取消弹窗
   */
  cancel() {
    this.setData({
      showModal: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const moneyTotal = "￥" + this.data.money;
    this.setData({
      isAliPay: false,
      money: 20,
      moneyTotal: moneyTotal
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})