const request = require('../../public/request');
const API = require("../../public/api");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    passwordType: true,
    eyeType: false,
    username: '',
    password: ''
  },

  /**
   * 是否显示密码
   */
  isShowPassword() {
    const eyeType = !this.data.eyeType;
    const passwordType = !this.data.passwordType;
    this.setData({
      passwordType: passwordType,
      eyeType: eyeType
    })
  },

  /**
   * 点击登录
   * @param {*} e 
   */
  formSubmit(e) {
    var username = e.detail.value.username; // 获取学号
    var password = e.detail.value.password; // 获取密码
    //校验
    if (username == "" || password == "") {
      wx.showModal({
        title: '提示',
        content: '不能为空',
      })
    } else {
      var that = this;
      // 发送登录请求
      wx.request({
        url: API.login, // 请求链接
        header: {
          // 'content-type': 'application/json' 默认值get方式
          'content-type': 'application/x-www-form-urlencoded' // post方式
        },
        timeout: 10000, // 超时时间
        method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT 请求方式
        data: { // 请求数据
          username: username,
          password: password
        },
        success: function (res) { // 请求成功
          if (res.data.code == 200) { // 登录成功
            // 保存登录信息
            wx.setStorageSync('studentInfo', res.data.data);
            wx.showToast({
              title: "登录成功！",
              duration: 3000
            })
            // 跳转到首页
            wx. switchTab({      
              url: '/pages/index/index'
            });    
            
          } else {
            wx.showToast({
              title: res.data.message,
              duration: 3000
            })
          }
        },
        fail: function () {
          wx.showToast({
            title: "网络连接失败",
            duration: 3000
          })
        },
      });
    }
  },

  async getPhoneNumber(e) {
    console.log(e.detail);
    if ("getPhoneNumber:fail no permission" == e.detail.errMsg) {
      wx.showModal({
        title: '提示',
        content: '该功能暂不支持使用'
      });
      return;
    }
    wx.login({
      //成功放回
      success: async (res) => {
        console.log(res);
        let code = res.code
        var data = {
          code: code,
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        }
        let result = await request.post(API.loginByWX, data);
        console.log("result");
        console.log(result);
        if (result.data.message == "操作成功") { // 登录成功
          // 保存登录信息
          wx.setStorageSync('studentInfo', result.data.data);
          wx.showToast({    
            title: "登录成功！",
            duration: 3000
          })
          // 跳转到首页
          wx. switchTab({      
            url: '/pages/index/index'
          });    
        } else {
          console.log(result);
          wx.showToast({
            title: result.data.message,
            duration: 3000
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})