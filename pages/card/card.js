// pages/card/card.js
const API = require("../../public/api");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money: 0.0,
    moneyToPay: 0.0,
    showModal: false
  },

  /**
   * 监控输入金额
   * @param {*} e 
   */
  watchMoney(e) {
    this.setData({
      moneyToPay: e.detail.value
    })
  },

  /**
   * 开始支付
   */
  wxPay() {
    let moneyToPay = this.data.moneyToPay;
    if (moneyToPay <= 0) {
      wx.showModal({
        title: '提示',
        content: '请输入正确的金额！'
      })
    } else if (moneyToPay >= 10000) {
      wx.showModal({
        title: '提示',
        content: '充值的金额太大！'
      })
    } else {
      this.setData({
        showModal: true
      })
    }
  },

  /**
   * 取消弹窗
   */
  cancel() {
    this.setData({
      showModal: false
    })
  },

  /**
   * 充值，提交表单
   */
  formSubmit() {
    var that = this;
    var username = wx.getStorageSync('studentInfo').sno;
    var money = this.data.moneyToPay;
    wx.request({
      url: API.cardPay,
      header: {
        // 'content-type': 'application/json' 默认值get方式
        'content-type': 'application/x-www-form-urlencoded' // post方式
      },
      timeout: 3000,
      method: 'put', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      data: {
        username: username,
        money: money
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.code == 200) {
          wx.showToast({
            title: '支付成功',
            image: '../../images/pay_success.png',
            duration: 3000,
            mask: true
          })
          that.setData({
            showModal: false
          })
        } else {
          wx.showToast({
            title: "修改失败",
            duration: 3000
          });
        }
        that.onLoad();
      },
      fail: function () {
        wx.showToast({
          title: "网络连接失败",
          duration: 3000
        })
      },
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that = this;
    var sno = wx.getStorageSync('studentInfo').sno;
    wx.request({
      url: API.getStudentInfo,
      header: {
        'content-type': 'application/json'
      },
      data: {
        sno: sno
      },
      timeout: 3000,
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      success: function (res) {
        if (res.statusCode == 200) {
          that.setData({
            money: res.data.data.money
          })
        }
      },
      fail: function () {
        wx.showToast({
          title: "网络连接失败",
          duration: 3000
        })
      },
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})