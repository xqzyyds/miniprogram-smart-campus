const request = require('../../public/request');
const API = require("../../public/api");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    sname: '',
    hasUserInfo: false,
    canIUseGetUserProfile: false,
    showModal: false,
    old_password: ''
  },

  /**
   * 跳转到个人资料页面
   */
  toInformation() {
    wx.navigateTo({
      url: '../information/information'
    })
  },

  /**
   * 点击修改密码，跳出弹窗
   */
  updatePassword() {
    this.setData({
      showModal: true
    })
  },

  /**
   * 取消修改密码，关闭弹窗
   */
  cancel() {
    this.setData({
      showModal: false
    })
    this.triggerEvent('cancel')
  },

  /**
   * 确认修改密码，提交表单
   * @param {*} e 
   */
  formSubmit(e) {
    var username = wx.getStorageSync('studentInfo').sno;
    if (username == "" || username == null) {
      wx.redirectTo({
        url: '../login/login',
      })
      return;
    }
    //显示加载界面
    wx.showLoading({ // 显示加载中loading效果 
      title: "加载中",
      mask: true //开启蒙版遮罩
    });
    // 原密码
    var old_password = e.detail.value.old_password;
    // 新密码
    var new_password = e.detail.value.new_password;
    // 新密码确认
    var new_password_confirm = e.detail.value.new_password_confirm;
    //校验
    if (old_password == "" || new_password == "" || new_password_confirm == "") { // 输入是否为空
      wx.showModal({
        title: '提示',
        content: '不能为空',
      })
    } else if (new_password != new_password_confirm) { // 两次密码是否一致
      wx.showModal({
        title: '提示',
        content: '两次密码不一样',
      })
    } else { // 输入不为空以及两次密码输入一致
      var that = this;
      // 发送请求，修改密码
      wx.request({
        url: API.updatePassword,
        header: {
          // 'content-type': 'application/json' 默认值get方式
          'content-type': 'application/x-www-form-urlencoded' // post方式
        },
        timeout: 3000,
        method: 'put', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        data: {
          username: username,
          oldPassword: old_password,
          newPassword: new_password
        },
        success: function (res) {
          console.log(res.data);
          if (res.data.code == 200) { // 修改成功
            that.setData({
              showModal: false
            });
            wx.showToast({
              title: "修改成功",
              duration: 3000
            });
          } else { // 修改失败
            wx.showToast({
              title: "修改失败",
              duration: 3000
            });
          }
        },
        fail: function () {
          wx.showToast({
            title: "网络连接失败",
            duration: 3000
          })
        },
      });
    }
    //隐藏加载界面
    wx.hideLoading();
  },

  /**
   * 跳转到一卡通页面
   */
  toCard() {
    wx.navigateTo({
      url: '../card/card'
    })
  },

  /**
   * 跳转到消息中心
   */
  toMessage() {
    wx.navigateTo({
      url: '../message/message'
    })
  },

  async getPhoneNumber(e) {
    console.log(e.detail);
    if ("getPhoneNumber:fail no permission" == e.detail.errMsg) {
      wx.showModal({
        title: '提示',
        content: '该功能暂不支持使用'
      });
      return;
    }
    wx.login({
      //成功放回
      success: async (res) => {
        console.log(res);
        let code = res.code
        let username = wx.getStorageSync('studentInfo').sno;
        var data = {
          username: username,
          code: code,
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        }
        let result = await request.post(API.bindWx, data);
        console.log(result);
      }
    })
  },

  // 新接口
  getUserProfile() {
    wx.getUserProfile({
      desc: '获取用户信息',
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        wx.setStorageSync('userInfo', res.userInfo);
      }
    });
  },

  /**
   * 退出登录
   */
  exit() {
    // 删除微信绑定的用户缓存
    wx.removeStorage({
      key: 'userInfo',
      success: (res) => {
        this.setData({
          hasUserInfo: false,
          userInfo: {},
          sname: ''
        })
        console.log(res)
      }
    });
    // 删除登录的学生信息缓存
    wx.removeStorageSync('studentInfo');
    // 删除课程表缓存
    wx.removeStorageSync('termData')
    // 跳转到登录页
    wx.reLaunch({
      url: '../login/login',
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 从缓存获取微信绑定信息以及学生登录信息
    const userInfo = wx.getStorageSync('userInfo');
    const studentInfo = wx.getStorageSync('studentInfo');
    const sname = studentInfo.sname;
    this.setData({
      hasUserInfo: !!userInfo, // 是否进行微信绑定
      userInfo: userInfo,
      sname: sname
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})