// pages/information/information.js
const API = require("../../public/api");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasUserInfo: false,
    avatar: '',
    faculty: '',
    grade: '',
    major: '',
    money: 0.0,
    open_id: '',
    phone: '',
    room_name: '',
    sex: true,
    sname: '',
    sno: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const userInfo = wx.getStorageSync('userInfo'); // 
    const studentInfo = wx.getStorageSync('studentInfo'); // 从缓存中获取学生信息
    this.setData({
      userInfo: userInfo,
    })
    this.setData({
      faculty: studentInfo.faculty,
      grade: studentInfo.grade,
      major: studentInfo.major,
      money: studentInfo.money,
      phone: studentInfo.phone,
      sex: studentInfo.sex,
      sname: studentInfo.sname,
      sno: studentInfo.sno
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})