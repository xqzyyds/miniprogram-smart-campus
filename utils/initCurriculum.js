const request = require('../public/request');
const API = require('../public/api');

/**
 * 根据学号获取课表信息, 构建一个学期的课表
 * @param {*} sno 
 * @param {*} termId 
 */
const updateCurriculum = async (sno, termId) => {
  console.log(termId);
  // 设置参数
  let data = {
    sno: sno,
    id: termId
  };
  // 发送请求，从后端得到数据
  const {
    data: res
  } = await request.get(API.getCurriculum, data);
  console.log(res.data);
  // 课表数据
  const backendData = res.data;
  // 初始化学期数组
  const termData = new Array(23).fill(null).map(() => []);

  // 将后端数据按学期存储
  for (let i = 0; i < backendData.length; i++) {
    const courseData = backendData[i];
    const weeks = Object.keys(courseData.course);

    for (let j = 0; j < weeks.length; j++) {
      const weekIndex = parseInt(weeks[j]) - 1;
      const courseSchedule = courseData.course[weeks[j]];
      const dayKeys = Object.keys(courseSchedule);

      for (let k = 0; k < dayKeys.length; k++) {
        const dayIndex = parseInt(dayKeys[k]) - 1;
        const classTimes = courseSchedule[dayKeys[k]];

        for (let l = 0; l < classTimes.length; l++) {
          const classInfo = classTimes[l];

          if (classInfo !== null) {
            termData[weekIndex][l * 7 + dayIndex] = {
              sub: courseData.courseName,
              add: courseData.classroom,
              tec: `@${courseData.teacher}`,
              color: '#fad0c4',
              type: 1
            };
          }
        }
      }
    }
  }
  // 获取构建好的课程表存入缓存
  wx.setStorageSync('termData', termData)
  return termData;
}

/**
 * 构建某一周的课表
 * @param {*} week 
 */
const getCurricumOfWeek = async(week, term) => {
  // 一周的课表数据
  var termData = wx.getStorageSync('termData');
  if (termData == null || termData == '') {
    var studentInfo = wx.getStorageSync('studentInfo');
    if (studentInfo == null || studentInfo == '') {
      wx.showModal({
        title: '提示',
        content: '请先登录',
      });
      return;
    }
    var sno = studentInfo.sno;
    termData = await updateCurriculum(sno, term.id);
  }
  var schedulePre = [];
  if (week < termData.length) {
    schedulePre = termData[week];
  }
  console.log(week);
  console.log(schedulePre);
  var schedule = [
    [],
    [],
    [],
    [],
    []
  ];
  // 空数据，表示没课
  var nullData = {
    sub: '',
    add: '',
    tec: "",
    color: '',
    type: 0,
  };
  var count = 0;
  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 7; j++, count++) {
      if (count < schedulePre.length && schedulePre[count] != null) {
        schedule[i][j] = schedulePre[count];
      } else {
        schedule[i][j] = nullData;
      }
    }
  }
  console.log(schedule);
  return schedule;
}

/**
 * 获取当前学期
 * @param {*} data 
 */
const getCurrentTerm = async(data) => {
  // 获取当前时间戳
  const currentTime = new Date().getTime();

  // 遍历学期数据
  for (const term of data) {
      const startTime = term.startTime;
      const endTime = term.endTime;

      // 判断当前时间是否在学期范围内
      if (currentTime >= startTime && currentTime <= endTime) {
          return term; // 返回当前学期
      }
  }
  return null; // 如果没有匹配的学期
}

module.exports = {
  updateCurriculum,
  getCurricumOfWeek,
  getCurrentTerm
};