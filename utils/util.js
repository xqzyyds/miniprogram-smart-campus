const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

/**
 * 格式化日期
 * @param {*} time 
 */
const formateDate = (time) => {
  let year = time.getFullYear();
  let month = time.getMonth() + 1 < 10 ? '0' + (time.getMonth() + 1) : (time.getMonth() + 1);
  let day = time.getDate() < 10 ? '0' + time.getDate() : time.getDate();
  return year + '-' + month + '-' + day;
}

/**
 * 获取当前日期一周内的时间
 * @param {*} data 日期Date
 */
const getCurrWeekList = (data) => {
  //根据日期获取本周周一~周日的年-月-日
  let weekList = [],
    date = new Date(data);
  //获取当前日期为周一的日期
  date.setDate(date.getDay() == "0" ? date.getDate() - 6 : date.getDate() - date.getDay() + 1);
  // push周一数据
  weekList.push(formateDate(date));
  console.log(weekList)
  //push周二以后日期
  for (var i = 0; i < 6; i++) {
    date.setDate(date.getDate() + 1);
    weekList.push(formateDate(date));
  }
  return weekList;
}
//["2022-08-08", "2022-08-09", "2022-08-10", "2022-08-11", "2022-08-12", "2022-08-13", "2022-08-14"]

/**
 * 计算今天是第几周
 * @param {*} startDate 开学日期，格式为 'YYYY-MM-DD'
 * @param {*} today 今天的日期，格式为 'YYYY-MM-DD'
 */
const getCurrentWeekNumber = (startDate, today) => {
  const startDateTime = new Date(startDate).getTime();
  const todayDateTime = new Date(today).getTime();

  // 计算开学到今天的毫秒数差值
  const timeDiff = todayDateTime - startDateTime;

  // 计算经过的周数
  const weekNumber = Math.ceil(timeDiff / (7 * 24 * 60 * 60 * 1000));

  return weekNumber;
};

/**
 * 获取距离开学时间第week周的那一周
 * @param {*} startDate 开学日期，格式为 'YYYY-MM-DD'
 * @param {*} week 第几周
 */
const getWeekByNumber = (startDate, week) => {
  const startDateTime = new Date(startDate).getTime();

  // 计算距离开学时间的毫秒数差值
  const timeDiff = (week - 1) * 7 * 24 * 60 * 60 * 1000;

  // 计算目标周的日期
  const targetDateTime = startDateTime + timeDiff;

  // 获取目标周的周一日期
  const targetWeekStartDate = new Date(targetDateTime);
  targetWeekStartDate.setDate(targetWeekStartDate.getDate() - targetWeekStartDate.getDay() + 1);

  // 获取目标周的周日日期
  const targetWeekEndDate = new Date(targetWeekStartDate);
  targetWeekEndDate.setDate(targetWeekEndDate.getDate() + 6);

  // 返回目标周的日期范围
  return {
    startDate: formateDate(targetWeekStartDate),
    endDate: formateDate(targetWeekEndDate)
  };
};

/**
 * 获取距离开学时间第week周的那一周
 * 返回格式化后的数据，包括日期、星期和是否为当前日期
 * @param {*} startDate 开学日期，格式为 'YYYY-MM-DD'
 * @param {*} week 第几周
 */
const getFormattedWeekByNumber = (startDate, week) => {
  const {
    startDate: weekStartDate,
    endDate: weekEndDate
  } = getWeekByNumber(startDate, week);
  const weekList = [];

  let currentDate = new Date(weekStartDate);
  while (currentDate <= new Date(weekEndDate)) {
    const formattedDate = formateDate(currentDate);
    weekList.push({
      day: [formattedDate.split('-')[1], formattedDate.split('-')[2]].join('-'),
      week: "星期" + "日一二三四五六".charAt(currentDate.getDay()),
      isCurr: formateDate(new Date()) === formattedDate
    });
    currentDate.setDate(currentDate.getDate() + 1);
  }

  return weekList;
};

/**
 * 获取距离开学时间第 week 周的那个月份
 * 返回格式化后的月份，格式为 'MM'
 * @param {*} startDate 开学日期，格式为 'YYYY-MM-DD'
 * @param {*} week 第几周
 */
const getMonthByNumber = (startDate, week) => {
  const {
    startDate: weekStartDate
  } = getWeekByNumber(startDate, week);

  const month = formateDate(new Date(weekStartDate)).slice(5, 7);

  return month;
};

const convertTimestampToFormattedDate = (timestamp) => {
  // 创建一个新的 Date 对象，并传入时间戳
  const date = new Date(timestamp);

  // 获取年、月和日
  const year = date.getFullYear();
  // 月份是从 0 开始的，所以要加 1
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');

  // 拼接成指定格式的日期字符串
  const formattedDate = `${year}-${month}-${day}`;

  return formattedDate;
}


module.exports = {
  formatTime,
  getCurrWeekList,
  formateDate,
  getCurrentWeekNumber,
  getWeekByNumber,
  getFormattedWeekByNumber,
  getMonthByNumber,
  convertTimestampToFormattedDate
};